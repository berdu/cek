# Apakah website Anda turun dari halaman Google 1 ke mana-mana?

![gambar https](http://rigor.com/wp-content/uploads/2017/01/https.png)

Apakah Anda memeriksa peringkat Anda, dan perlahan-lahan akan turun atau tidak muncul sama sekali?

Saya menduga Anda ingin mencari tahu apa sih mungkin menyebabkan peringkat situs Anda menurun.

## Mengapa Drop Site di Google

Algoritma Google yang ditujukan untuk membantu orang menemukan"berkualitas tinggi"situs dengan mengurangi peringkat konten berkualitas rendah.

Tapi peringkat dapat berfluktuasi untuk beberapa alasan.  Kadang-kadang karena kesalahan manusia dan dicegah.

Daftar cepat mengapa situs menjatuhkan:
- Pelacakan kata kunci yang salah
- Update algoritma Google baru-baru ini
- website baru
- konten berkualitas rendah
- link yang berkualitas rendah
- Kehilangan link berkualitas tinggi
- hosting murah
- hukuman Google
- Pesaing berusaha lebih keras
- lokasi bisnis pindah

## Apa yang bisa kau lakukan?

Saya selalu memiliki ranking bagus untuk website saya, itu muncul di bagian atas halaman satu dan saya mendapatkan banyak bisnis.  Kemudian sesuatu terjadi!

Bagaimana ini bisa terjadi?  Dan apa yang dapat Anda lakukan untuk memperbaiki masalah dan mendapatkan lalu lintas dan mengarah terjadi lagi?

Sayangnya, kesalahan SEO dapat terjadi.

Magang, situs yang diretas, konten yang buruk, masalah seluler dan salah urus dapat merusak situs dan menghancurkan bisnis tanpa peringatan.

Setelah kerusakan kerugian besar dan mengganggu bisnis, pertempuran sebagai pemilik situs baru saja dimulai.

Di bawah ini adalah lebih mendalam penjelasan tentang apa yang dapat Anda lakukan untuk meningkatkan peringkat Anda.

### Periksa Konten Anda

Konten masih raja.  Sementara ada unsur-unsur lain dari SEO penting untuk mencapai puncak mesin pencari, konten berkualitas tinggi tetap di bagian atas daftar.

Google mengatakan:
konten berkualitas rendah pada beberapa bagian dari situs web dapat memengaruhi keseluruhan peringkat situs, dan dengan demikian menghilangkan halaman berkualitas rendah, penggabungan atau meningkatkan isi halaman dangkal individu menjadi halaman lebih berguna, atau memindahkan halaman berkualitas rendah untuk domain yang berbeda akhirnya bisa membantu  peringkat konten berkualitas tinggi Anda.

### Link wajar
Efek dari skema link berkualitas rendah adalah diam tapi mematikan.  pemilik usaha tak terduga dapat dikenakan sanksi tersebut.

perusahaan SEO tidak etis membangun link balik miskin di situs pihak ketiga spam untuk menghasilkan hasil jangka pendek untuk klien.  Agak seperti steriods untuk situs web.

Tapi menyadari, tim spam web Google dapat mengambil tindakan manual pada link yang tidak wajar.

Ketika tindakan manual diambil, Google akan memberitahukan melalui [Webmaster Tools](https://www.google.com/webmasters/).  Jika Anda memiliki peringatan untuk link yang tidak wajar ke situs Anda, yang terbaik untuk menghapus link tersebut dan meminta pertimbangan ulang a.

Anda dapat memperbaiki tidak wajar link dengan memastikan mereka tidak lulus PageRank.  Untuk melakukan hal ini, tambahkan rel ="nofollow"atau menghapus link seluruhnya.

Setelah memperbaiki atau menghapus link tidak wajar ke situs Anda, biarkan Google tahu tentang perubahan Anda dengan mengirimkan permintaan pertimbangan ulang di Search Console.

### Masalah SEO Umum

Jika Anda menangani SEO untuk situs Anda, menyelidiki daerah-daerah kunci akan membantu Anda menendang pantat besar dalam memperbaiki masalah seo Anda.

Kurangnya komunikasi: Sebelum Anda panik, periksa dengan departemen pemasaran Anda, itu departemen atau di-rumah webmaster.  Sebuah seo memperbaiki sederhana mungkin menjadi salah satu email pergi.

Situs kesalahan perayapan: kesalahan website dapat mencegah situs Anda tidak muncul di mesin pencari.  Gunakan Google Webmaster Tools untuk menemukan kesalahan crawl situs Anda.

Cari perubahan mesin algo: Belajar tentang algoritma dapat membantu menunjukkan fluktuasi terbaru dalam situs Anda peringkat.  Moz memiliki catatan sejarah besar dari update Google.

Update situs baru-baru ini: Jika situs Anda baru-baru ini diperbarui oleh anggota tim, yang mungkin mereka tidak sengaja memicu kegagalan seo.

Update CMS Terbaru: situs WordPress teratur menjalani lebar sistem pembaruan perangkat lunak untuk memperbaiki bug dan eksploitasi.  Jika situs Anda memiliki fitur khusus, mungkin update terbaru yang dipicu kesalahan seo.

Nama domain kadaluarsa: Ini mungkin kartu kredit Anda telah berakhir dan Anda Domain Registrar tidak bisa mengisi kartu Anda untuk memperbarui domain Anda.

### SEO Masalah Teknis

Setelah memeriksa kesehatan besar gambar SEO Anda, waktu untuk menemukan dan memperbaiki masalah.  Sebagian besar masalah SEO adalah karena kesalahan manusia.

Rel = kanonikal: rel = fungsi tag canonical sebagai 301 redirect untuk mesin pencari tanpa pembaca mengalihkan secara fisik.

link nofollow: Jika digunakan di tempat yang salah itu link nofollow dapat menyebabkan kerusakan dengan tidak lewat di sepanjang jus link ke halaman yang lebih dalam.

Meta Robots: Tidak termasuk halaman individu dari hasil pencarian.

Robots.txt: Sebuah file teks yang diciptakan oleh webmaster untuk memberitahu robot mesin pencari cara merangkak dan mengindeks situs Anda.

Judul halaman: Verifikasi visibilitas mereka dalam hasil pencarian dan dalam kode sumber Anda.

H1 tags: Verifikasi tag H1 Anda berisi kata kunci yang relevan untuk menggambarkan halaman Anda.

Melupakan Gambar Tags: Pastikan setiap gambar di situs Web Anda dioptimalkan untuk dicari dan diindeks oleh laba-laba pencarian dengan memasukkan tag ALT pada setiap gambar.

situs AJAX: Kadang-kadang mereka dapat melayani mesin pencari konten yang salah.  Hal ini biasa terjadi dengan pembangun website seperti Wix.

Hreflang: Verifikasi tag bahasa Anda melayani halaman yang benar.

link navigasi: Pastikan Anda belum mengubah navigasi lebar situs Anda.  Kurangnya link ke halaman yang lebih dalam dapat menghambat penempatan mereka dalam hasil pencarian.

Internal-link: Konsep yang sama seperti di atas namun terkait dengan link dalam konten halaman.

Meta Deskripsi: Digunakan untuk menggambarkan isi halaman untuk pengunjung dalam hasil pencarian.  Memverifikasi visibilitas mereka dalam hasil pencarian dan dalam kode sumber Anda.

Shared Hosting: Kadang-kadang server turun dan tetangga melakukan kejahatan.  Periksa dengan penyedia hosting Anda dan pastikan situs Anda tidak berbagi IP yang sama dengan tetangga teduh.

Https / SSL: masalah Duplikat konten dapat timbul jika Anda melayani kedua situs aman dan tidak aman untuk mesin pencari.

plugin gagal: Bila tidak konsisten dipertahankan, plugin dapat memiliki efek Cascading pada sisa website Anda.

Kecepatan situs: Google menggunakan kecepatan situs sebagai SEO Peringkat Faktor-tidak satu besar, tapi pasti salah satu yang penting.

pengaturan WordPress membaca: Kunjungi pengaturan membaca di dashboard WordPress Anda.  Pastikan Anda tidak diklik Search Engine tombol Visibilitas yang mengatakan Mencegah mesin pencari mengindeks situs ini.

### Pencegahan SEO

Mencegah lebih baik daripada mengobati.  SEO pencegahan adalah sesuatu yang semua orang membenci, apakah mobil Anda yang, rumah atau SEO, tidak ada yang bisa lebih mahal daripada membayar uang sebelum nya bangkrut.

Tapi sering membayar untuk memperbaiki masalah jauh lebih.

Beruntung bagi pemasar online, mudah untuk menghindari bencana SEO dengan mengikuti beberapa langkah.

On-site diagnostik: Secara teratur menentukan kesehatan dasar dari SEO situs Anda.

On-site monitoring: Situs Anda aktif 24 jam sehari.  Mengetahui jika situs Anda turun sangat penting untuk kesehatan jangka panjang dari SEO Anda.

Cari update algoritma: Google membuat pembaruan besar setiap tahun.  Mengetahui tentang update tersebut dapat memberikan pemahaman yang lebih baik tentang mengapa hasil pencarian Anda berfluktuasi.

email: Komputer tidak pernah tidur.  Penjadwalan email adalah cara yang bagus untuk memantau kesehatan situs Anda.

pelatihan praktek terbaik: perubahan SEO secara triwulanan.  pendidikan terus sangat penting untuk kesehatan situs Anda dan kampanye SEO.

### Tools SEO

Pikirkan di tempat diagnostik seperti menjalankan tes darah.  Banyak masalah situs memiliki dengan SEO tidak terlihat dengan mata telanjang.

Menggunakan alat pelaporan SEO dapat membantu Anda mengidentifikasi dan memperbaiki masalah sebelum efek SERPs Anda.

Memantau kesehatan dan stabilitas situs Anda sangat penting untuk pemasaran masuk.

alat yang sama digunakan untuk sukses patokan dapat digunakan untuk membendung bencana.

Alat untuk menggunakan:

Google Analytics: Pantau adanya perubahan lalu lintas utama dan halus.

Google Webmaster Tools: Gunakan dashboard analytics pencarian untuk patokan posisi Anda saat ini dengan posisi bulan sebelumnya.  Gunakan ini untuk menentukan apakah Anda memiliki isseus yang harus ditangani.

Jetpack Pemberitahuan: situs WordPress memiliki keuntungan dari memanfaatkan peringatan up-time melalui Jetpack.  semakin cepat Anda tahu tentang situasi semakin banyak waktu Anda harus memperbaiki itu.

Analyzer situs: Saya akan menghasilkan laporan SEO cepat dan sederhana untuk setiap halaman situs Anda.

MozCast: Jika Google terserah sesuatu yang Anda dapat yakin bahwa MozCast akan mengingatkan Anda.

SEMRush: Mungkin Anda ingin mempelajari lebih lanjut tentang situs web Anda.  Mungkin Anda ingin memata-matai pesaing.  SEMRush memungkinkan Anda untuk melakukan keduanya.

Raven Tools: Raven menawarkan berbagai alat SEO, termasuk auditor website populer.

DeepCrawl: Temukan kesempatan yang hilang dengan struktur situs Anda, halaman yatim, dan efek SEO pada UX.

Search Engine Berita dan Update

Menjaga tab pada kesehatan pencarian Anda merupakan tugas penting.  Ini termasuk memantau kinerja peringkat Anda sendiri tetapi juga melacak update algoritma pencarian baru-baru ini.

fluktuasi besar dalam peringkat pencarian adalah tanda-tanda dari masalah yang lebih besar.  Ini bisa menjadi masalah SEO di tempat teknis atau off-site SEO masalah terkait dengan update algo Google baru-baru ini.

Di bidang SEO cepat, melanjutkan pendidikan penting untuk pertumbuhan karir dan perbaikan.  Mempertahankan pelatihan praktek terbaik dalam SEO, adalah sama pentingnya dengan industri lainnya.

Setelah Anda membuat beberapa perubahan SEO kunci untuk website Anda, Anda akan segera menyadari bahwa Anda hanya menyentuh puncak gunung es.

Berurusan dengan mesin pencari optimasi yang luas (SEO) kerusakan dan kerugian keuangan dapat menjadi tugas menakutkan, terutama bagi pemilik yang tidak siap, atau asing, dengan menemukan kesalahan SEO.

Untuk pendalaman melebih alasan kenapa ranking anda di Google turun anda bisa melihat lebih lanjut di: [Kenapa Ranking Saya di Google Turun?](https://berdu.id/blog/kenapa-ranking-toko-online-di-google-turun)
